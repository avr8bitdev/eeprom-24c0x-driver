/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "EEPROM_functions.h"
#include "../../MCAL_drivers/I2C_driver/I2C_functions.h"
#include "../../basic_includes/bit_manip.h"
#include <util/delay.h>


// --- internal --- //
static u8 EEPROM_baseAddr = 0; // base address + pins A2 - A0

static u8 EEPROM_u8WriteAddr(const u8 u8PageAddrCpy, const u8 u8ByteAddrCpy)
{
    // repeat until we're Master on data bus
    register I2C_action_t master_action = I2C_master_action_Arbitration_lost;

    for (u8 i = 255; (i > 0) && (master_action != I2C_master_action_Start); i--)
    {
        I2C_u8MasterStartCond(); // start
        master_action = I2C_enumMasterGetLastAction();
    }
    if (master_action != I2C_master_action_Start)
        return 0;

    // write slave addr
    I2C_u8MasterSetSlaveAddrW(EEPROM_baseAddr | u8PageAddrCpy);
    if (I2C_enumMasterGetLastAction() != I2C_master_action_SLA_W_transmitted_ACK)
        return 0;

    // write byte addr
    I2C_u8MasterSendData(u8ByteAddrCpy);
    if (I2C_enumMasterGetLastAction() != I2C_master_action_Data_transmitted_ACK)
        return 0;

    return 1;
}
// ---------------- //

void EEPROM_vidInit(const u8 u8BaseAddrCpy, const u8 u8PinA2Cpy, const u8 u8PinA1Cpy, const u8 u8PinA0Cpy)
{
    EEPROM_baseAddr = (u8BaseAddrCpy << 3);

    BIT_ASSIGN(EEPROM_baseAddr, 2, u8PinA2Cpy);
    BIT_ASSIGN(EEPROM_baseAddr, 1, u8PinA1Cpy);
    BIT_ASSIGN(EEPROM_baseAddr, 0, u8PinA0Cpy);

    I2C_vidMasterInit(100e3); // 100kHz I2C speed
}

u8 EEPROM_u8WriteByte(const u8 u8DataCpy, const u8 u8PageAddrCpy, const u8 u8ByteAddrCpy)
{
    // write page and byte addr
    if (!EEPROM_u8WriteAddr(u8PageAddrCpy, u8ByteAddrCpy))
    {
        I2C_vidMasterStopCond(1);
        return 0;
    }

    // write byte data
    I2C_u8MasterSendData(u8DataCpy);
    if (I2C_enumMasterGetLastAction() != I2C_master_action_Data_transmitted_ACK)
    {
        I2C_vidMasterStopCond(1);
        return 0;
    }

    I2C_vidMasterStopCond(1);

    _delay_ms(5); // write cycle time

    return 1;
}

u8 EEPROM_u8ReadByte(const u8 u8PageAddrCpy, const u8 u8ByteAddrCpy)
{
    // dummy write (move address pointer)
    // write page and byte addr
    if (!EEPROM_u8WriteAddr(u8PageAddrCpy, u8ByteAddrCpy))
    {
        I2C_vidMasterStopCond(1);
        return 0;
    }

     // repeated start
    I2C_u8MasterStartCond();
    if (I2C_enumMasterGetLastAction() != I2C_master_action_Repeated_Start)
    {
        I2C_vidMasterStopCond(1);
        return 0;
    }

    // get byte data
    I2C_u8MasterSetSlaveAddrR(EEPROM_baseAddr | u8PageAddrCpy);

    if (I2C_enumMasterGetLastAction() != I2C_master_action_SLA_R_transmitted_ACK)
    {
        I2C_vidMasterStopCond(1);
        return 0;
    }

    // trigger action flag to retrieve byte
    register u8 result = I2C_u8MasterGetDataByTrigger(0);

    // TODO: check this again
    if (I2C_enumMasterGetLastAction() != I2C_master_action_Data_received_NACK)
    {
        I2C_vidMasterStopCond(1);
        return 0;
    }

    // trigger action flag to retrieve next byte
    // otherwise stop condition won't work properly
    // TODO: figure out why!
//    I2C_u8GetDataByTrigger();

    I2C_vidMasterStopCond(1);

    return result;
}

